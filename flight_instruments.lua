script_name("Flight Instruments")
script_description("Provides basic flight instruments for flying vehicles")
script_author("Miko-chan")
script_moonloader(26)

local moonAdds = require "MoonAdditions"
local fltCommon = require "flight_instruments_common"

local readings = fltCommon.readings
local controls = fltCommon.controls
local gauges = {
    airSpeed = 0.0,
    radarAltitude = 0.0,
    radioAltitudeLong = 0.0,
    radioAltitudeShort = 0.0,
}

function main()
    print("Executing main loop...")
    
    local fltTextures = createFlightInstrumentTextures()
    
    while true do
        wait(0)
        if fltCommon.isPlayerInAircraft() then
            displayCarNames(false)
            local currentVehicle = getCarCharIsUsing(PLAYER_PED)
            setVehicleNameText(currentVehicle)

            while true do
                wait(0)
                if fltCommon.isPlayerInVehicle() then
                    renderVehicleNameText()
                    updateGlobalReadingsState(currentVehicle)

                    -- Compute airspeed needle angle
                    local airSpeedNeedleAngle = readings.airSpeed
                    if readings.airSpeed <= 28.05 then
                        airSpeedNeedleAngle = airSpeedNeedleAngle * 0.223
                    elseif readings.airSpeed <= 50.09 then
                        airSpeedNeedleAngle = (airSpeedNeedleAngle * 1.1) - 23.0
                    elseif readings.airSpeed <= 233.26 then
                        airSpeedNeedleAngle = (airSpeedNeedleAngle * 1.75) - 55.0
                    else
                        airSpeedNeedleAngle = 352.0
                    end
                    updateGaugeValue("airSpeed", getDrawCircleAngle(airSpeedNeedleAngle))
                    print(readings.airSpeed, gauges.airSpeed)

                    -- Compute radar altitude needle angle
                    local radarAltitudeNeedleAngle = fltCommon.feetToMeters(readings.radarAltitude)
                    if readings.radarAltitude <= 500.10 then
                        radarAltitudeNeedleAngle = radarAltitudeNeedleAngle * 1.181
                    elseif readings.radarAltitude <= 2500.62 then
                        radarAltitudeNeedleAngle = (radarAltitudeNeedleAngle * 0.146) + 157.5
                    else
                        radarAltitudeNeedleAngle = 268.75
                    end
                    updateGaugeValue("radarAltitude", getDrawCircleAngle(radarAltitudeNeedleAngle))
                    
                    -- Compute radio altitude needle angles
                    local radioAltitudeNeedleAngle = fltCommon.feetToMeters(readings.radioAltitude) * 1.181 
                    updateGaugeValue("radioAltitudeLong", getDrawCircleAngle(radioAltitudeNeedleAngle))
                    updateGaugeValue("radioAltitudeShort", getDrawCircleAngle(radioAltitudeNeedleAngle / 10))
                    
                    -- Draw the instruments
                    drawTextureScaled(fltTextures.airSpeedDial, 349, 571, 179, 165)
                    drawTextureScaled(fltTextures.longNeedle, 347, 571, 179, 165, gauges.airSpeed)
                    
                    
                    
                else
                    markCarAsNoLongerNeeded(currentVehicle)
                    break
                end
            end

            displayCarNames(true)
        end
    end
end

local vehicleNameText = fltCommon.DisplayText:new("FLTVEHP", {
    style = fltCommon.textStyle.MENU,
    scale = { 0.64, 2.2 },
    color = { 57, 105, 45 },
    posAlign = fltCommon.textPosAlign.RIGHT,
    defaultDrawPos = { 625.0, 285.0 },
})
local vehicleNameTextTimer = fltCommon.Timer.new()
local vehicleNameTextFadeOut = false

function setVehicleNameText(currentVehicle)
    vehicleNameText.visible = true
    vehicleNameText.fxtEntry = getNameOfVehicleModel(getCarModel(currentVehicle))
    vehicleNameText:resetFade()
    vehicleNameTextTimer:reset()
    vehicleNameTextFadeOut = false
end

function renderVehicleNameText()
    if vehicleNameText:fadeIn(1000) and vehicleNameTextTimer:waitFor(2000) and not vehicleNameTextFadeOut then
        vehicleNameText:resetFade()
        vehicleNameTextFadeOut = true
    elseif vehicleNameTextFadeOut and vehicleNameText:fadeOut(1000) then
        vehicleNameText.visible = false
    end
    vehicleNameText:drawText()
end

function updateGlobalReadingsState(currentVehicle)
    local x, y, z = getCharCoordinates(PLAYER_PED)

    readings.airSpeed = fltCommon.mpsToKnots(getCarSpeed(currentVehicle))
    readings.radarAltitude = fltCommon.metersToFeet(getCharHeightAboveGround(PLAYER_PED))
    readings.radioAltitude = fltCommon.metersToFeet(z)
    readings.bankAngle = getCarRoll(currentVehicle)
    readings.pitch = getCarPitch(currentVehicle)
    readings.zoneId = getNameOfZone(x, y, z)
    
    controls.landingGearUp = getPlaneUndercarriagePosition(currentVehicle) <= 0
end

function updateGaugeValue(gaugeType, newValue)
    local interpolation = 2.5
    local gaugeValue = gauges[gaugeType]
    if gaugeValue > newValue then
        gaugeValue = math.max(newValue, gaugeValue - interpolation)
    else
        gaugeValue = math.min(newValue, gaugeValue + interpolation)
    end
    gauges[gaugeType] = gaugeValue
end


function createFlightInstrumentTextures()
    local basePath = getWorkingDirectory() .. "/resource/flight_instruments/textures/"
    
    function texturePathOf(name)
        return basePath .. name .. ".png"
    end
    
    return {
        airSpeedDial        = assert(moonAdds.load_png_texture(texturePathOf("AirspeedDial"))),
        autopilotKnob       = assert(moonAdds.load_png_texture(texturePathOf("AutopilotKnob"))),
        autopilotLight      = assert(moonAdds.load_png_texture(texturePathOf("AutopilotLight"))),
        computerLit         = assert(moonAdds.load_png_texture(texturePathOf("ComputerLit"))),
        computerUI          = assert(moonAdds.load_png_texture(texturePathOf("ComputerUI"))),
        gearDown            = assert(moonAdds.load_png_texture(texturePathOf("GearDown"))),
        gearUp              = assert(moonAdds.load_png_texture(texturePathOf("GearUp"))),
        health              = assert(moonAdds.load_png_texture(texturePathOf("Health"))),
        longNeedle          = assert(moonAdds.load_png_texture(texturePathOf("LongNeedle"))),
        radarAltDial        = assert(moonAdds.load_png_texture(texturePathOf("RadarAltDial"))),
        radioAltDial        = assert(moonAdds.load_png_texture(texturePathOf("RadioAltDial"))),
        radioAltShortNeedle = assert(moonAdds.load_png_texture(texturePathOf("RadioAltShortNeedle"))),
    }
end

function getDrawCircleAngle(angle)
    return fltCommon.clamp(math.fmod(angle, 360), 0, 360)
end

function drawTextureScaled(texture, x, y, width, height, angle, r, g, b, a)
    local resX, resY = getScreenResolution()
    local scaleX, scaleY = resX / 1366, resY / 768
    
    x = x * scaleX
    y = y * scaleY
    width = width * scaleX
    height = height * scaleY
    r = r or 255
    g = g or 255
    b = b or 255
    a = a or 255
    angle = angle or 0
    
    texture:draw(x, y, x + width, y + height, r, g, b, a, 0 - angle)
end
